###Original Text Taken from http://lincgeek.org/bashpodder/

BashPodder Was written and is maintained here by me (Linc) with the generous code contributions and great ideas of many of its users, some of whom have contributed modified scripts of their own. BashPodder is currently licensed under the GPL. BashPodder was written to be small and fast, and most importantly, to conform to the KISS rule (Keep It Simple Stupid). That way, anyone can add to and detract from the script to suit their own needs (and they are welcome to do so). BashPodder is listed in many places as a "Linux" podcatching client, and in fact, that is what I wrote it for initially, however, it should be noted that I have dozens of emails telling me how well it works on everything else, including but not limited to MaxOSX, Solaris, AIX, Net Open and FreeBSD, even windows and many other OS's I have forgotten I am sure.

Bashpodder requires you have bash (but I have heard it runs fine under korn as well), wget and sed.
As a bare minimum, you'll need the following three files:
bashpodder.shell - The main program
parse_enclosure.xsl - xsl style sheet to grab enclosure info
bp.conf - The example podcast config list (containing some of my personal favorites)

 By Linc 10/1/2004
 Find the latest script at http://lincgeek.org/bashpodder
 Revision 1.21 12/04/2008 - Many Contributers!
 If you use this and have made improvements or have comments
 drop me an email at linc dot fessenden at gmail dot com
 and post your changes to the forum at http://lincgeek.org/lincware
 I'd appreciate it!


### End of Original Information

### Fork Information

 Changes by Rev. Fr. Robert Bower April 29, 2021
 Renamed the script executable to bashpodder

 Added a option to skip download but add podcasts to logfile to be used when you are using bash podder with a podcast where you don't want to download all past episodes. Supply nd to bashpodder as an argument and the podcasts will be added to the logfile but not actually downloaded. Remove from the logfile any episodes you want to download and rerun bashpodder without the nd argument.

 Example:

 bashpodder nd 
 Will not actually download the podcasts but will add them to the log file.  Handy to use when you want bashpodder to get caught up with where you are at in a podcast.

